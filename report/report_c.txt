NOTE: ~10 chars per line

10000 lines:
  letter_count:
    Children |   Self | Function
    ---------+--------+-------------------------------
      86.26% |  6.97% | _PyEval_EvalFrameDefault
       5.37% |  5.12% | ucs1lib_fastsearch
       3.97% |  3.77% | _Py_DECREF
       1.31% |  3.67% | PyType_HasFeature
       4.15% |  3.07% | lookdict_unicode_nodummy
       1.13% |  2.74% | _Py_INCREF
       3.80% |  2.51% | _PyObject_IS_GC
       2.10% |  2.04% | dictkeys_get_index
       0.60% |  1.88% | _Py_IS_TYPE
       3.34% |  1.65% | pymalloc_alloc
       0.51% |  1.61% | _PyRuntimeState_GetThreadState
      10.97% |  1.38% | r_object
       1.29% |  1.26% | siphash24
       1.24% |  1.24% | _nv013409rm
       2.26% |  1.23% | lookdict_unicode
      86.01% |  1.11% | call_function
       2.42% |  0.96% | _Py_XDECREF
       0.51% |  0.96% | _PyObject_GC_TRACK_impl
       1.52% |  0.92% | _PyType_Lookup
       5.35% |  0.91% | find_name_in_mro
       3.25% |  0.84% | insertdict
       0.90% |  0.84% | resolve_slotdups
       2.43% |  0.81% | PyUnicode_New
       0.70% |  0.70% | address_in_range
       3.75% |  0.69% | _PyDict_GetItem_KnownHash
      85.38% |  0.66% | _PyEval_EvalCode
       0.86% |  0.62% | r_long
       ...

  letter_count_row:
    Children |   Self | Function
    ---------+--------+-------------------------------
      96.97% | 17.36% | _PyEval_EvalFrameDefault
       5.34% |  5.16% | _Py_DECREF
       5.15% |  3.56% | lookdict_unicode_nodummy
       1.36% |  3.32% | _Py_INCREF
       0.71% |  2.89% | PyType_HasFeature
      96.93% |  2.59% | call_function
       0.88% |  2.49% | _Py_IS_TYPE
       0.48% |  1.82% | _PyRuntimeState_GetThreadState
       1.73% |  1.73% | ucs1lib_fastsearch
       3.43% |  1.71% | vgetargs1_impl
       4.50% |  1.69% | tupledealloc
       1.78% |  1.48% | pymalloc_alloc
       1.48% |  1.47% | dictkeys_get_index
       3.94% |  1.21% | _PyTuple_FromArray
       1.26% |  1.14% | _PyObject_GC_TRACK_impl
       0.94% |  1.02% | _PyObject_GC_UNTRACK_impl
       ...

9255770 lines:
  letter_count:
    Children |   Self | Function
    ---------+--------+-------------------------------
      90.38% | 88.06% | ucs1lib_fastsearch
       4.99% |  4.57% | _Py_bytes_lower
       1.68% |  1.11% | ascii_decode
       0.96% |  0.96% | hpet_clkevt_set_next_event
       1.47% |  0.38% | asm_exc_page_fault
       0.20% |  0.20% | clear_page_rep
      99.74% |  0.17% | _PyEval_EvalFrameDefault
       0.15% |  0.15% | __irqentry_text_start
       0.93% |  0.13% | handle_mm_fault
       0.40% |  0.12% | get_page_from_freelist
       ...

  letter_count_row:
    Δεν τολμάω να το τρέξω! :-)
