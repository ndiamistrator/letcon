# -*- coding: utf-8 -*-

# NOTE: εναλλακτικά μπορείς να τρέξεις python3 -m letcon ...

from letcon.main import parse, main

args, kwargs = parse()
main(*args, **kwargs)
