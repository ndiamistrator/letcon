# -*- coding: utf-8 -*-

import io
import sys
import contextlib

__all__ = ('patched',)
# NOTE: το __all__ χρησιμεύει για καλή ανάγνωση (σαν export/public) ή για ένα
#       statement για του οποίου τη χρήση οι python προγραμματιστές
#       αποθαρρύνουν, καθότι δυσκολεύει στην ανάγνωση και την επεξεργασία:
#         from X import *
# NOTE: προσοχή στο comma στα literal tuple με 1 item, αφού:
#         type( ('patched') ).__name__ == 'str'
# NOTE: χρησιμοποιώ tuple όταν δεν έχω ανάγκη για mutable γιατί είναι
#       γρηγορότερα και μικρότερα από list

@contextlib.contextmanager
def patched_output():
    ''' Patch sys.stdout in a context

        Returns the patched io.StringIO
    '''
    _stdout = sys.stdout
    stdout = io.StringIO(_stdout.encoding)
    sys.stdout = stdout
    try:
        # NOTE: ειδική χρήση iterator για contextlib.contextmanager:
        yield stdout
    finally:
        sys.stdout = _stdout
        stdout.seek(0) # NOTE: για να το διαβάσω μετά
