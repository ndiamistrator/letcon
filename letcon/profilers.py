# -*- coding: utf-8 -*-

import sys

import cProfile
import timeit as _timeit

from .timer import Timer

from .mod.pstats import Stats

from .patch_output import patched_output


__all__ = ('timer', 'timeit', 'profile')


def timer(method, *args, quiet=False):
    t=Timer()
    t.start()
    if quiet:
        with patched_output():
            method(*args)
    else:
        method(*args)
    return t.stop()


def timeit(method, *args, quiet=False):
    def M():
        with patched_output() as stdout:
            # NOTE: θέλουμε το output από το τελευταίο loop, όχι απ' όλα!
            M.stdout = stdout # NOTE: κάπου πρέπει να το αποθηκεύσω!
            method(*args)
    timer = _timeit.Timer('M()', globals=locals())
    loops, elapsed_time = timer.autorange()
    if not quiet:
        sys.stdout.write(M.stdout.read())
    print(f"Elapsed time: {elapsed_time/loops:0.4f} seconds ({loops} loop{'s' if loops>1 else ''})")
    return elapsed_time


def profile(method, *args, quiet=False):
    profile = cProfile.Profile()
    if quiet:
        with patched_output():
            profile.runcall(method, *args)
    else:
        profile.runcall(method, *args)
    stats = Stats(profile)
    stats.sort_stats('time')
    stats.print_stats(percent=True, unit=1)
    elapsed_time = stats.total_tt
    print(f"Elapsed time: {elapsed_time:0.4f} seconds")
    return elapsed_time
