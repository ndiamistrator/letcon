# -*- coding: utf-8 -*-

__all__ = ('letter_count_row_noop',)


def read_in_row(filename):
  for row in open(filename,"r"):
    yield row

def letter_count_row_noop(arxeio,language):
  if language=='latin':
    letters='abcdefghijklmnopqrstuvwxyz'
  elif language=='greek':
    letters='αβγδεζηθικλμνξοπρστυφχψω'

  print('\n'+language.upper(),'LETTER COUNTER in rows\n')

  count={}
  orig=0
  sum=0
  val=0
  lines=0
  count = dict.fromkeys(letters,0)

  EMPTY = '' # XXX

  for row in read_in_row(arxeio):
    orig+=len(row)
    lines+=1

    if orig>0:

      keimfold=EMPTY.casefold() # XXX

      for let in letters:

        val=EMPTY.count(let) # XXX

        sum+=int(val)
        count[let]+=val

  print(orig,'input characters')
  print(sum,'usable -', round(100*sum/orig,1),'%')
  if sum>0:
    for key,val in count.items():
        perc=str(round(100*val/sum,1))
        dots='.'*(len(str(sum))-len(perc)-len(str(val))+5)
        print(f"\t{key} : {val} {dots} {perc} %")
