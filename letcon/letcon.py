# -*- coding: utf-8 -*-

__all__ = ('letter_count', 'letter_count_row')


def letter_count(arxeio,language):
  if language=='latin':
    letters='abcdefghijklmnopqrstuvwxyz'
  elif language=='greek':
    letters='αβγδεζηθικλμνξοπρστυφχψω'

  print('\n'+language.upper(),'LETTER COUNTER\n')

  f=open(arxeio,"r")
  keimeno=f.read()
  f.close()

  orig=len(keimeno)
  print(orig,'input characters')
  if orig>0:
    count={}
    keimfold=keimeno.casefold() #metraei kanonika kai to teliko sigma
    sum=0
    val=0
    for let in letters:
      val=keimfold.count(let)
      sum+=int(val)
      count[let]=val

    print(sum,'usable -', round(100*sum/orig,1),'%')
    if sum>0:
      for key,val in count.items():
        perc=str(round(100*val/sum,1))
        dots='.'*(len(str(sum))-len(perc)-len(str(val))+5)
        print(f"\t{key} : {val} {dots} {perc} %")


def read_in_row(filename):
  for row in open(filename,"r"):
    yield row

def letter_count_row(arxeio,language):
  if language=='latin':
    letters='abcdefghijklmnopqrstuvwxyz'
  elif language=='greek':
    letters='αβγδεζηθικλμνξοπρστυφχψω'

  print('\n'+language.upper(),'LETTER COUNTER in rows\n')

  count={}
  orig=0
  sum=0
  val=0
  lines=0
  count = dict.fromkeys(letters,0)

  for row in read_in_row(arxeio):
    orig+=len(row)
    lines+=1

    if orig>0:
      keimfold=row.casefold() #metraei kanonika kai to teliko sigma
      for let in letters:
        val=keimfold.count(let)
        sum+=int(val)
        count[let]+=val

  print(orig,'input characters')
  print(sum,'usable -', round(100*sum/orig,1),'%')
  if sum>0:
    for key,val in count.items():
        perc=str(round(100*val/sum,1))
        dots='.'*(len(str(sum))-len(perc)-len(str(val))+5)
        print(f"\t{key} : {val} {dots} {perc} %")
