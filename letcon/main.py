import sys
import argparse

from . import letcon
from . import letcon_noop
from . import profilers

from .patch_output import patched_output


__all__ = ('parse', 'main')


LANGUAGES = ('latin', 'greek')


METHODS = {method:getattr(letcon, method) for method in letcon.__all__}
METHODS.update((method, getattr(letcon_noop, method)) for method in letcon_noop.__all__)
PROFILERS = {profiler:getattr(profilers, profiler) for profiler in profilers.__all__}


def parse(argv=None):
    help_methods = '|'.join(METHODS)
    help_profilers = '|'.join(PROFILERS)
    default_profiler = tuple(PROFILERS)[0]

    if argv is None:
        argv = sys.argv
    program, args = argv[0], argv[1:]
    parser = argparse.ArgumentParser(program)

    parser.add_argument('--nocache', '-n',
        action = 'store_true')

    parser.add_argument('--profiler', '-p',
        default=default_profiler,
        help=f'{help_profilers} (default:{default_profiler})')

    parser.add_argument('--method', '-m',
        help=f'{help_methods}  (default:ALL)')

    parser.add_argument('--quiet', '-q',
        action = 'store_true')

    parser.add_argument('input')

    parser.add_argument('language')

    args = parser.parse_args(args)

    profiler = PROFILERS[args.profiler]

    if not args.method:
        methods = dict(METHODS) # NOTE: copy it, just in case
    else:
        method = METHODS[args.method]
        methods = {args.method: method}

    if not args.nocache:
        with open(args.input) as f: f.read()

    return (profiler, methods, args.input, args.language), {'quiet':args.quiet}


def main(profiler, methods, *args, quiet=False):
    for name, method in methods.items():
        print(f'\nProfiling {name}...')
        profiler(method, *args, quiet=quiet)
