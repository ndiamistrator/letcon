# -*- coding: utf-8 -*-

# NOTE: το __main__.py είναι απαραίτητο για να μπορεί να τρέξει το package σαν
#       script υποστηρίζοντας relative imports

from .main import parse, main

args, kwargs = parse()
main(*args, **kwargs)
